#include <stdio.h>
int attendees(int price){
return 120-(price-15)/5*20;
}
int cost(int price){
return 500+attendees(price)*3;
}
int revenue(int price){
return price*attendees(price);
}
int profit(int price){
return revenue(price)-cost(price);
}
int main(){
    printf("\t\t....Profit Table....\n\n");
    int price;
for(price=5;price<=50;price+=5){
    printf("Ticket Price = Rs.%d\t Total Profit = Rs.%d\n",price,profit(price));
    printf("...............................................\n\n");
}
return 0;
}
